#pragma once
#include "employee.h"

class Engineer : public employee, public WorkTime, public Project
{
protected:
	int base;
	string project;
	double participation;
	int projBudget;
public:
    Engineer(int id, string name, int worktime, int base, string project, double participation, int projBudget) :
		employee(id, name, worktime)
	{
		this->base = base;
		this->project = project;
		this->projBudget = projBudget;
		this->participation = participation;
	}
	int payForHours(int worktime, int base)
	{
		return (worktime * base);
	}
	double payForProject(int projBudget, double participation)
	{
		return (projBudget*participation);
	}
	void cal�Payment()
	{
		setPayment(payForHours(getWorktime(), base) + payForProject(projBudget, participation));
	}
  ~Engineer() {}
};

class  Programmer: public Engineer
{
public:
	Programmer(int id, string name, int worktime, int base, string project, double participation, int projBudget) :
		Engineer(id, name, worktime, base, project, participation, projBudget) {}
	~Programmer() {}
};

class Tester: public Engineer
{
public:
	Tester(int id, string name, int worktime, int base, string project, double participation, int projBudget) :
		Engineer(id, name, worktime, base, project, participation, projBudget) {}
	~Tester() {}
};

class TeamLeader: public Programmer, public Heading
{
	int NumOfSubordinates;
	int PayForOne;
public:
	TeamLeader(int id, string name, int worktime, int base, string project, double participation, int projBudget, int NumOfSubordinates, int PayForOne) :
		Programmer(id, name, worktime, base, project, participation, projBudget)
	{
		this->NumOfSubordinates = NumOfSubordinates;
		this->PayForOne = PayForOne;
	}
	~TeamLeader() {}

	int payForSubordinates(int PayForOne, int NumOfSubordinates) 
	{
		return PayForOne*NumOfSubordinates;
	}
	void cal�Payment()
	{
		setPayment(payForHours(getWorktime(), base) + payForProject(projBudget, participation) + payForSubordinates(PayForOne, NumOfSubordinates));
	}
};


