#pragma once
#include "employee.h"
class Manager :	public employee, public Project
{
protected:
	string project;
	double participation;
	int projBudget;
public:
	Manager(int id, string name, int worktime, string project, double participation, int projBudget) :
		employee(id, name, worktime)
	{
		this->project = project;
		this->projBudget = projBudget;
		this->participation = participation;
	}
	double payForProject(int projBudget, double participation)
	{
		return (projBudget*participation);
	}
	void cal�Payment()
	{
		setPayment(payForProject(projBudget, participation));
	}
	~Manager() {}
};

class ProjectManager: public Manager, public Heading
{
protected:
	int NumOfSubordinates;
	int PayForOne;

public:
	ProjectManager(int id, string name, int worktime, string project, double participation, int projBudget, int NumOfSubordinates, int PayForOne) :
		Manager(id, name, worktime, project, participation, projBudget)
	{
		this->NumOfSubordinates = NumOfSubordinates;
		this->PayForOne = PayForOne;
	}
	void cal�Payment()
	{
		setPayment(payForProject(projBudget, participation) + payForSubordinates(PayForOne, NumOfSubordinates));
	}
	int payForSubordinates(int PayForOne, int NumOfSubordinates)
	{
		return PayForOne*NumOfSubordinates;
	}
	~ProjectManager() {}
};

class SeniorManager: public ProjectManager
{
public:
	SeniorManager(int id, string name, int worktime, string project, double participation, int projBudget, int NumOfSubordinates, int PayForOne) :
		ProjectManager(id, name, worktime, project, participation, projBudget, NumOfSubordinates, PayForOne) {}
	void cal�Payment()
	{
		setPayment(payForProject(projBudget, participation) + payForSubordinates(PayForOne, NumOfSubordinates)*1.5);
	}
	~SeniorManager() {}
};

