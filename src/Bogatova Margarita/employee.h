#pragma once

class employee
{
private:
	int id;
	string name;
	int worktime;
	double payment;

public:
	employee(int id, string name, int worktime)
	{
		this->id = id;
		this->name = name;
		this->worktime = worktime;
		this->payment = 0;
	}
	virtual void cal�Payment()=0;

	int getWorktime()
	{
		return worktime;
	}
	void setPayment(double payment) 
	{
		this->payment = payment;
	}
	int getId()
	{
		return id;
	}
	string getName()
	{
		return name;
	}
	double getPayment()
	{
		return payment;
	}
};
class WorkTime
{
public:
	virtual int payForHours(int worktime, int base) = 0;
};

class Project
{
public:
	virtual double payForProject(int projBudget, double participation) = 0;
};

class Heading
{
public:
	virtual int payForSubordinates(int PayForOne, int NumOfSubordinates) = 0;
};

