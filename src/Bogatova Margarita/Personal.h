#pragma once
#include "employee.h"
class Personal : public employee, public WorkTime
{
private:
	int base;

public:
    Personal(int id, string name, int worktime, int base) :
		employee(id, name, worktime), base(base) {};

	int payForHours(int worktime, int base)
	{
		return (worktime * base);
	}
	void cal�Payment()
	{
		setPayment(payForHours(getWorktime(), base));
	}
};

class Cleaner: public Personal
{
public:
	Cleaner(int id, string name, int worktime, int base) :
		Personal(id, name, worktime, base) {};
	~Cleaner() {}
};

class Driver: public Personal
{
public:
	Driver(int id, string name, int worktime, int base) :
		Personal(id, name, worktime, base) {};
	~Driver() {}
};


