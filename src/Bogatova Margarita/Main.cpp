#include <fstream>
#include <iostream>
#include <vector>
#include <string>
using  namespace std;


#include"employee.h"
#include "Engineer.h"
#include "Manager.h"
#include "Personal.h"

int main()
{
	vector <employee*> Employee;
	ifstream file("base.txt", ios_base::in);
	if (!file.is_open()) // ���� ���� ����� ������
	{
		cout << "base.txt �� ����� ���� ������\n";
		file.close();
		return 1;
	}
	char  str[256] = { 0 };
	int i;
	while (!file.eof())
	{
		i = 0;
		int tempId = 0;
		string tempfio;
		string position;

		file >> tempId;

		file >> str;
		tempfio = str;
		tempfio += ' ';
		file >> str;
		tempfio += str;
		file >> str;
		tempfio += str;

		file >> str;
		position = str;

		int worktime;
		file >> worktime;
		if (position == "Driver")
		{
			int base;
			file >> base;
			Employee.push_back(new Driver(tempId, tempfio, worktime, base));
			continue;
		}
		if (position == "Cleaner")
		{
			int base;
			file >> base;
			Employee.push_back(new Cleaner(tempId, tempfio, worktime, base));
			continue;
		}
		if (position == "Programmer")
		{
			int base;
			file >> base;
			char project[20];
			file >> project;
			double participation;
			file >> participation;
			int Budget;
			file >> Budget;
			Employee.push_back(new Programmer(tempId, tempfio, worktime, base, project, participation, Budget));
			continue;
		}
		if (position == "Tester")
		{
			int base;
			file >> base;
			char project[20];
			file >> project;
			double participation;
			file >> participation;
			int Budget;
			file >> Budget;
			Employee.push_back(new Tester(tempId, tempfio, worktime, base, project, participation, Budget));
			continue;
		}
		if (position == "TeamLeader")
		{
			int base;
			file >> base;
			int NumOfSubordinates;
			file >> NumOfSubordinates;
			char project[20];
			file >> project;
			double participation;
			file >> participation;
			int PayForOne;
			file >> PayForOne;
			int Budget;
			file >> Budget;
			Employee.push_back(new TeamLeader(tempId, tempfio, worktime, base, project, participation, Budget, NumOfSubordinates, PayForOne));
			continue;
		}
		if (position == "Manager")
		{
			char project[20];
			file >> project;
			double participation;
			file >> participation;
			int Budget;
			file >> Budget;
			Employee.push_back(new Manager(tempId, tempfio, worktime, project, participation, Budget));
			continue;
		}
		if (position == "ProjectManager")
		{
			int NumOfSubordinates;
			file >> NumOfSubordinates;
			char project[20];
			file >> project;
			double participation;
			file >> participation;
			int PayForOne;
			file >> PayForOne;
			int Budget;
			file >> Budget;
			Employee.push_back(new ProjectManager(tempId, tempfio, worktime, project, participation, Budget, NumOfSubordinates, PayForOne));
			continue;
		}
		if (position == "SeniorManager")
		{
			int NumOfSubordinates;
			file >> NumOfSubordinates;
			char project[20];
			file >> project;
			double participation;
			file >> participation;
			int PayForOne;
			file >> PayForOne;
			int Budget;
			file >> Budget;
			Employee.push_back(new SeniorManager(tempId, tempfio, worktime, project, participation, Budget, NumOfSubordinates, PayForOne));
			continue;
		}
	}

	file.close();
	for (int i = 0; i < Employee.size(); ++i) 
	{
		Employee[i]->cal�Payment();
		cout <<"Id: "<< Employee[i]->getId() << endl;
		cout << "Name: " << Employee[i]->getName() << endl;
		//cout << "Payment: "<<Employee[i]->getPayment() << endl;
		printf("Payment: %0.0f \n", Employee[i]->getPayment());
		cout << endl;
	}
	system("pause");
}